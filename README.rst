ghc-api-compat
==============

GHC-API isn't stable. Especially since we have been renaming every module to put
them in the ``GHC.*`` namespace (cf `#13009
<https://gitlab.haskell.org/ghc/ghc/issues/13009>`_).

This package aims to make GHC-API transitions easier by mapping old module names to
newer module names, by adding function aliases, etc. Pull requests are welcome!

Build with:

.. code::

   cabal build -w /path/to/ghc/HEAD/_build/stage1/bin/ghc

ghc-api.csv
===========

ghc-api.csv is a file chronicling GHC's module renamings. Right now it duplicates information from ghc-api-compat.cabal, and there's no guarantee that it will be kept up-to-date or in sync, but there's an aspiration that this package(and others) can be generated automatically from it, and it can serve as a 'single source of truth'.

Please note modules may have been renamed multiple times. In that case, there will be multiple entries, from A to B and from B to C.